# pier

Python Interpreter Executing Remotely

## How?

Run in a terminal:

```bash
$ ./pier.py
```

Open in a browser: http://localhost:9090/example1

Or via:

```bash
$ curl http://localhost:9090/example1
```

## Options

```bash
$ ./pier.py -h
usage: pier.py [-h] [-d DIR] [-H HOST] [-P PORT]

pier - Python Interpreter Executing Remotely

optional arguments:
  -h, --help            show this help message and exit
  -d DIR, --dir DIR     directory containing python scripts (default: ./scripts/)
  -H HOST, --host HOST  host name (default: localhost)
  -P PORT, --port PORT  port number (default: 9090)

```

